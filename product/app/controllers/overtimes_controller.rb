class OvertimesController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user! #, only: [:new, :create, :show]
  
  def index
    @overtimes = current_user.overtimes.all
  end

  def new
    @overtime = Overtime.new
  end

  def create
    @overtime = Overtime.new(overtime_params)
    @overtime.user_id = current_user.id

    if @overtime.save
      redirect_to @overtime, notice: "Your Overtime post was created successfully"
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @overtime.update(overtime_params)
      redirect_to @overtime, notice: "Your Overtime post was updated successfully"
    else
      render :edit
    end
  end

  def show
  end

  def destroy
    @overtime.delete
    redirect_to overtimes_path, notice: "Your Overtime post was deleted successfully"
  end

  private
    def overtime_params
      params.require(:overtime).permit(:date, :rationale, :status)
    end

    def set_post
      @overtime = Overtime.find(params[:id])
    end
end
