require "administrate/base_dashboard"

class UserDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    overtimes: Field::HasMany,

    id: Field::Number,
    email: Field::String.with_options(searchable: true),
    password: Field::String,

    first_name: Field::String,
    last_name: Field::String,
    type: Field::String,

    created_at: Field::DateTime,
    updated_at: Field::DateTime,

    #sign_in_count: Field::Number,
    #current_sign_in_at: Field::DateTime,
    #last_sign_in_at: Field::DateTime,
    #current_sign_in_ip: Field::String.with_options(searchable: false),
    #last_sign_in_ip: Field::String.with_options(searchable: false),

  }.freeze

  # INDEX List (default limit: 4)
  COLLECTION_ATTRIBUTES = %i[
    overtimes
    email
    type
    first_name
  ].freeze

  # SHOW attribs
  SHOW_PAGE_ATTRIBUTES = %i[
    email
    
    first_name
    last_name
    type
    
    created_at
    updated_at
    
    overtimes
  ].freeze


  # NEW-EDIT form fields
  FORM_ATTRIBUTES = %i[
    email
    password

    first_name
    last_name
  ].freeze

  # COLLECTION_FILTERS
  # a hash that defines filters that can be used while searching via the search
  # field of the dashboard.
  #
  # For example to add an option to search for open resources by typing "open:"
  # in the search field:
  #
  #   COLLECTION_FILTERS = {
  #     open: ->(resources) { resources.where(open: true) }
  #   }.freeze
  COLLECTION_FILTERS = {}.freeze

  # Overwrite this method to customize how users are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(user)
  #   "User ##{user.id}"
  # end
end
