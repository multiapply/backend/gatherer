module ApplicationHelper

  def active?(path)
    "active" if current_page?(path)
  end

  def thead(columns)
    content_tag :thead, class: 'thead-dark' do
      content_tag :tr do
        columns.collect {|column|  
          concat content_tag(:th,column)
        }.join().html_safe
      end
    end
  end
  
end
