require "active_support/core_ext/integer/time"

Rails.application.configure do
  # [0] Inspect Cucumber files
  config.annotations.register_directories('features')
  config.annotations.register_extensions('feature') { |tag| /#\s*(#{tag}):?\s*(.*)$/ }

  # (Precede configs at config/application.rb)

  # [1] Hot-Reload Code
  # Reload Code (between requests)
  config.cache_classes = false
  # Dont Load on memory (On Boot)
  config.eager_load = false
  # Full error reports.
  config.consider_all_requests_local = true

  # Cache
  if Rails.root.join('tmp', 'caching-dev.txt').exist?
    config.action_controller.perform_caching = true
    config.action_controller.enable_fragment_cache_logging = true

    config.cache_store = :memory_store
    config.public_file_server.headers = {
      'Cache-Control' => "public, max-age=#{2.days.to_i}"
    }
  else
    config.action_controller.perform_caching = false

    config.cache_store = :null_store
  end




  # [2] Active Storage
  # Upload to local FS (config/storage.yml)
  config.active_storage.service = :local

  
  # [3] Action Mailer (simulation,cache,local_url)
  config.action_mailer.raise_delivery_errors = false
  config.action_mailer.perform_caching = false
  config.action_mailer.default_url_options = { host: 'localhost', port: 3000 }


  # [4] Active Support 
  # Deprecations (Log, Exceptions, Disallow some)
  config.active_support.deprecation = :log
  config.active_support.disallowed_deprecation = :raise
  config.active_support.disallowed_deprecation_warnings = []


  # [5] Active Record
  # Pending migrations
  config.active_record.migration_error = :page_load
  # DB queries
  config.active_record.verbose_query_logs = true


  # [6] Assets
  # Disable concatenation and preproc of assets.
  # Delay View rendering (Complex Assets)
  config.assets.debug = true
  # Asset requests: not logged.
  config.assets.quiet = true


  # [7] Extra
  # Gem: Listen (async detect changes)
  config.file_watcher = ActiveSupport::EventedFileUpdateChecker
  # config.i18n.raise_on_missing_translations = true
  # config.action_view.annotate_rendered_view_with_filenames = true


  # [8] Action Cable
  # Access from any origin.
  # config.action_cable.disable_request_forgery_protection = true

end
