require "active_support/core_ext/integer/time"

Rails.application.configure do
  # (Precede configs at config/application.rb)

  # [1] Hot-Reload Code
  # Dont Reload Code (between requests)
  config.cache_classes = true
  # Load on memory (On Boot)
  config.eager_load = true
  # Full error reports
  config.consider_all_requests_local       = false

  # Cache
  config.action_controller.perform_caching = true

  # config.cache_store = :mem_cache_store


  
  
  # [2] Active Storage
  # Upload to local FS
  config.active_storage.service = :local
  
  # [2] Other
  # Dont serve static files at '/public'
  config.public_file_server.enabled = ENV['RAILS_SERVE_STATIC_FILES'].present?


  # [3] Action Mailer
  # Ignore bad emails (previous email server)
  # config.action_mailer.raise_delivery_errors = false
  config.action_mailer.perform_caching = false


  # [4] Active Support 
  # Deprecations (Notify, Log, Disallow some)
  config.active_support.deprecation = :notify
  config.active_support.disallowed_deprecation = :log
  config.active_support.disallowed_deprecation_warnings = []


  # [5] Active Record
  config.active_record.dump_schema_after_migration = false

  # [5] Middleware (auto-conn switching strategy)
  # Default: Store a last write timestamp in the session
  # Options to DatabaseSelector (time to wait after write)
  # config.active_record.database_selector = { delay: 2.seconds }
  # Choose DB by time delay
  # config.active_record.database_resolver = ActiveRecord::Middleware::DatabaseSelector::Resolver
  # Set timestamps for the last write (time to wait before read from replica)
  # config.active_record.database_resolver_context = ActiveRecord::Middleware::DatabaseSelector::Resolver::Session


  # [6] Assets
  # Compress CSS using a preprocessor.
  # config.assets.css_compressor = :sass

  # Do not fallback to assets pipeline if a (precompiled asset) is missed.
  config.assets.compile = false

  # Enable serving of images, stylesheets, and JavaScripts from an asset server.
  # config.asset_host = 'http://assets.example.com'


  # [7] Extra
  # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
  # the I18n.default_locale when a translation cannot be found).
  config.i18n.fallbacks = true




  # [8] Logs
  # Use default logging formatter so that PID and timestamp are not suppressed.
  config.log_formatter = ::Logger::Formatter.new

  # Use a different logger for distributed setups.
  # require "syslog/logger"
  # config.logger = ActiveSupport::TaggedLogging.new(Syslog::Logger.new 'app-name')

  if ENV["RAILS_LOG_TO_STDOUT"].present?
    logger           = ActiveSupport::Logger.new(STDOUT)
    logger.formatter = config.log_formatter
    config.logger    = ActiveSupport::TaggedLogging.new(logger)
  end

  # Include generic and useful information about system operation, but avoid logging too much
  # information to avoid inadvertent exposure of personally identifiable information (PII).
  config.log_level = :info

  # Prepend all log lines with the following tags.
  config.log_tags = [ :request_id ]



  # [9] Security
  # Ensure: ENV["RAILS_MASTER_KEY"] or config/master.key
  # config.require_master_key = true
  
  # Force all access to the app over SSL, use Strict-Transport-Security, and use secure cookies.
  # config.force_ssl = true


    
  # [10] Action Dispatch
  # Specifies the header that your server uses for sending files.
  # config.action_dispatch.x_sendfile_header = 'X-Sendfile' # for Apache
  # config.action_dispatch.x_sendfile_header = 'X-Accel-Redirect' # for NGINX


  

  # [11] Action Cable
  # Mount Action Cable outside main process or domain.
  # config.action_cable.mount_path = nil
  # config.action_cable.url = 'wss://example.com/cable'
  # config.action_cable.allowed_request_origins = [ 'http://example.com', /http:\/\/example.*/ ]

  # [12] Active Job
  # Use a real queuing backend for Active Job (and separate queues per environment).
  # config.active_job.queue_adapter     = :resque
  # config.active_job.queue_name_prefix = "product_production"


end
