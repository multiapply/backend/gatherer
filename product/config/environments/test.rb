require "active_support/core_ext/integer/time"

# The test environment is used exclusively to run your application's
# test suite. You never need to work with it otherwise. Remember that
# your test database is "scratch space" for the test suite and is wiped
# and recreated between test runs. Don't rely on the data there!

Rails.application.configure do
  # [0] Inspect Cucumber files
  config.annotations.register_directories('features')
  config.annotations.register_extensions('feature') { |tag| /#\s*(#{tag}):?\s*(.*)$/ }

  # (Precede configs at config/application.rb)

  # [1] Hot-Reload Code
  # Reload Code (between requests)
  config.cache_classes = false
  # Dont Load on memory (On Boot) - single test
  config.eager_load = false
  # Full error reports.
  config.consider_all_requests_local       = true

    
  # Cache
  config.action_view.cache_template_loading = true
  # Configure public file server for tests with Cache-Control for performance.
  config.public_file_server.enabled = true
  config.public_file_server.headers = {
    'Cache-Control' => "public, max-age=#{1.hour.to_i}"
  }
  config.action_controller.perform_caching = false
  config.cache_store = :null_store

  # Raise exceptions instead of rendering exception templates.
  config.action_dispatch.show_exceptions = false
  # Disable request forgery protection in test environment.
  config.action_controller.allow_forgery_protection = false


  

  # [2] Active Storage
  # Upload to local FS (in a temp dir)
  config.active_storage.service = :test


  # [3] Action Mailer
  config.action_mailer.perform_caching = false
  # Receive mails at array (ActionMailer::Base.deliveries).
  config.action_mailer.delivery_method = :test


  # [4] Active Support 
  # Deprecations (to_stderr, Exceptions, Disallow some)
  config.active_support.deprecation = :stderr
  config.active_support.disallowed_deprecation = :raise
  config.active_support.disallowed_deprecation_warnings = []


  # [7] Extra
  # config.i18n.raise_on_missing_translations = true
  # config.action_view.annotate_rendered_view_with_filenames = true
end
