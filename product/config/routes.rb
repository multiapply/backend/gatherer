Rails.application.routes.draw do
  resources :tasks do
    member do
      patch :up
      patch :down
    end
  end
  resources :projects
  namespace :admin do
    resources :users
    resources :admin_users
    resources :overtimes

    root to: "users#index"
  end
  devise_for :users, skip: [:registrations]
  resources :overtimes
  root to: 'static#homepage'
end
