class AddUsersToOvertimes < ActiveRecord::Migration[6.1]
  def change
    add_reference :overtimes, :user, null: false, foreign_key: true
  end
end
