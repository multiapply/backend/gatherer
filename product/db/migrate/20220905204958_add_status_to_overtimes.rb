class AddStatusToOvertimes < ActiveRecord::Migration[6.1]
  def change
    add_column :overtimes, :status, :integer, default: 0
  end
end
