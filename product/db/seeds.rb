@user = User.create(email: "test@test.com", password: "asdf123dedf", password_confirmation: "asdf123dedf", first_name: "Jon", last_name:"Snow")

puts "1 User created"

AdminUser.create(email: "admin@test.com", password: "asdf123dedf", password_confirmation: "asdf123dedf", first_name: "Admin", last_name:"User")

puts "1 Admin User created"

100.times do |overtime|
  Overtime.create!(date: Date.today, rationale: "#{overtime} ratinale content", user_id: @user.id)
end

puts "100 Overtime Posts have been created"