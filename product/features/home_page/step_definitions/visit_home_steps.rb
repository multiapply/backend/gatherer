Given('the employee is new to the platform') do
end

When('the employee gets into the website') do
  visit '/'
end

Then('the employee will see a welcome page') do
  expect(page).to have_selector '.title', text: 'Welcome to gatherer'
end
