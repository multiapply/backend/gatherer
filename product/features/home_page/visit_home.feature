Feature: Employees can visit the app
  In order to register their working day
  As an Employee
  I want to see the initial homepage

  Scenario: Enter the app
    Given the employee is new to the platform
    When the employee gets into the website
    Then the employee will see a welcome page