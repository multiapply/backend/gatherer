Feature: AdminUser can manage overtimes of its employees
  In order to have a clear vision of employees working time
  As an AdminUser
  I want to review overtime requests send by employees

  Scenario: Approve an overtime request
    Given the adminUser has entered the platform
      And the Employees has already send some overtimes
    When the adminUser approves an employee overtime request
    Then the overtime will be marked as approved