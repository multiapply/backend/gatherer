Feature: Employees can see his overtime requests
  In order to check all posted overtime requests
  As an Employee
  I want to see the list of all my overtimes

  Scenario: See list of Overtimes
    Given the employee has already send some overtimes
    When the employee gets into the Overtime Section
    Then the employee will see all his posted Overtimes
  
  Scenario: Edit Send Overview request
    Given the employee has already send an overtime
    When he updates that overtime
    Then the overtime should have the changes