Feature: Employees can request Overtime
  In order to be taken in consideration the overtime worked
  As an Employee
  I want to send an overtime request

  Scenario: Request new Overtime
    Given the employee has identified himself
    When the employee create the Overtime
    Then the employee will see the created Overtime title