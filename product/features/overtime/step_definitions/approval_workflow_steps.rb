Given('the adminUser has entered the platform') do
  @admin_user = create(:admin_user)
  login_as(@admin_user, :scope => :user)
end

Given('the Employees has already send some overtimes') do
  @overtime = create(:overtime)
end

When('the adminUser approves an employee overtime request') do
  visit edit_overtime_path(@overtime)

  within ".overtime__status" do
    choose('Approved')
  end
  click_on "Save"
end

Then('the overtime will be marked as approved') do
  expect(@overtime.reload.status).to eq('approved')
end