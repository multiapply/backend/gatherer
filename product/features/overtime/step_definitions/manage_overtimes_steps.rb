Given('the employee has already send some overtimes') do
  @user = User.create(email: "test@test.com", password: "asdf123dedf", password_confirmation: "asdf123dedf", first_name: "Jon", last_name:"Snow")
  login_as(@user, :scope => :user)

  overtime1 = Overtime.create(date: Date.today, rationale: "Overtime1", user_id: @user.id)
  overtime2 = Overtime.create(date: Date.today, rationale: "Overtime2", user_id: @user.id)
end


When('the employee gets into the Overtime Section') do
  visit overtimes_path
end

Then('the employee will see all his posted Overtimes') do
  expect(page).to have_content(/Overtime1|Overtime2/)
end



Given('the employee has already send an overtime') do
  @user = create(:second_user)
  login_as(@user, :scope => :user)

  overtime = create(:overtime, user_id: @user.id)
end

When('he updates that overtime') do
  visit overtimes_path

  within_table 'Overtimes' do
    within ".overtime" do
      click_link 'Edit'
    end
  end

  fill_in "overtime[date]", with: Date.today
  fill_in "overtime[rationale]", with: "Edited content"
  click_on "Save"
end

Then('the overtime should have the changes') do
  expect(page).to have_content("Edited content")
end

