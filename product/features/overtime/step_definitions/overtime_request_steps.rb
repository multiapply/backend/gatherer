Given('the employee has identified himself') do
  @user = User.create(email: "test@test.com", password: "asdf123dedf", password_confirmation: "asdf123dedf", first_name: "Jon", last_name:"Snow")
  login_as(@user, :scope => :user)
end

When('the employee create the Overtime') do
  visit new_overtime_path
  fill_in 'overtime[date]', with: Date.today
  fill_in 'overtime[rationale]', with: "Some rationale"
  
  click_on "Save"
end

Then('the employee will see the created Overtime title') do
  expect(page).to have_content("Some rationale")
  #expect(page).to have_selector '.details', text: 'Some rationale'
end
