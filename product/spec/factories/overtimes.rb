FactoryBot.define do
  factory :overtime do
    date { Date.today }
    rationale { "Some Rationale" }
    user
    trait :from_second_user do
      association :user, factory: :second_user
    end
  end
  
  factory :second_overtime, class: "Overtime" do
    date { Date.yesterday }
    rationale { "Some Content2" }
    association :user, factory: :second_user
  end
end
