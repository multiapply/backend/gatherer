FactoryBot.define do
  sequence :email_2 do |n|
    "test#{n}@test.com"
  end

  factory :user do
    first_name {"John"}
    last_name {"Snow"}
    
    sequence(:email) { |n| "test#{n}@example.com" }
    password { "password" }

    factory :second_user do
      #email { "test@test.com" }
      email { generate :email_2 }
      password { "asdf123dedf" }
    end
  end

  factory :admin_user, class: "AdminUser" do
    first_name {"Admin"}
    last_name {"User"}
    #sequence(:email) { |n| "admin#{n}@user.com" }
    email { generate :email_2 }
    password { "asdf123dedf" }
  end  
  
end
