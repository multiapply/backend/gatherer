require 'rails_helper'

describe 'navigate' do
  before do
    @admin_user = create(:admin_user)
    login_as(@admin_user, :scope => :user)
  end

  describe "edit" do
    before do
      @overtime = create(:overtime)
      visit edit_overtime_path(@overtime)
    end
    it "has a status that can be edited on the form by an admin" do

      #choose('overtime_status_approved')
      choose(option: 'approved')
      click_on "Save"

      expect(@overtime.reload.status).to eq('approved') 
    end

    it 'cannot be edited by a non admin' do
      logout(:user)
      user = create(:user)
      login_as(user, :scope => :user)

      visit edit_overtime_path(@overtime)
      
      expect(page).to_not have_selector('.overtime__status')
    end
  end
  
end