require 'rails_helper'

describe 'navigate' do
  before do
    #let!(:user) { create(:user) }
    @user = create(:second_user)
    login_as(@user, :scope => :user)
  end
  describe 'index' do
    before do
      visit overtimes_path
    end
    it 'can be reached successfully' do
      expect(page.status_code).to eq(200)
    end

    it "has a title of Overtimes" do
      expect(page).to have_content(/Overtimes/)
      #expect(page).to have_selector '.title', text: 'Overtimes'
    end
    it "has a list of posted overtimes" do
      overtime1 = build_stubbed(:overtime)
      overtime2 = build_stubbed(:second_overtime)
      visit overtimes_path
      expect(page).to have_content(/Rationale|Content2/)
    end
    
  end

  describe 'new' do
    it 'has a link from the homepage' do
      visit root_path

      click_link("new_overtime_from_nav")
      expect(page.status_code).to eq(200) 
    end
  end

  describe 'creation' do
    before do
      visit new_overtime_path
    end

    it 'has a new form that can be reached' do
      expect(page.status_code).to eq(200)
    end

    it 'can be created from new form page' do
      fill_in 'overtime[date]', with: Date.today
      fill_in 'overtime[rationale]', with: "Some rationale"

      click_on "Save"
      expect(page).to have_content("Some rationale")
    end

    it 'will have a user associated with it' do
      fill_in 'overtime[date]', with: Date.today
      fill_in 'overtime[rationale]', with: "User Association"

      click_on "Save"
      expect(User.last.overtimes.last.rationale).to eq("User Association")
    end

  end

  describe 'edit' do
    before do
      @overtime = create(:overtime, user_id: @user.id)
    end

    it 'can be reached by clicking edit on index page' do
      visit overtimes_path
      
      within_table 'Overtimes' do
        within ".overtime" do
          click_link 'Edit'
        end
      end
      
      #expect(current_path).to eq(edit_overtime_path(overtime))
      expect(page).to have_current_path(edit_overtime_path(@overtime))
      expect(page.status_code).to eq(200)
    end

    it 'can be edited' do
      visit edit_overtime_path(@overtime)

      fill_in 'overtime[date]', with: Date.today
      fill_in 'overtime[rationale]', with: "Edited content"
      click_on "Save"

      expect(page).to have_content("Edited content")
    end

  end

  describe 'delete' do
    it 'can be deleted' do
      @overtime = create(:overtime, user_id: @user.id)
      visit overtimes_path

      click_link "delete_overtime_#{@overtime.id}"
      expect(page.status_code).to eq(200) 
      expect(page).to_not have_content(@overtime.rationale)
    end
    
  end

end