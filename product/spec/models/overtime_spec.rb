require 'rails_helper'

RSpec.describe Overtime, type: :model do
  describe "Creation" do
    before do
      @overtime = create(:overtime)
    end
    
    it 'can be created' do
      expect(@overtime).to be_valid 
    end

    it 'cannot be created without a date and rationale' do
      @overtime.date = nil
      @overtime.rationale = nil
      expect(@overtime).to_not be_valid 
    end

  end
end
