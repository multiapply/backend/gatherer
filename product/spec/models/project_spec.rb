require 'rails_helper'

RSpec.describe Project do

  it_should_behave_like 'sizeable'

  describe 'completeness' do

    describe 'without a task' do
      let(:project){ build_stubbed(:project) }
      
      it 'considers a project with no tasks to be done' do
        expect(project).to be_done
      end

      it 'properly estimates a blank project' do
        expect(project.completed_velocity).to eq(0)
        expect(project.current_rate).to eq(0)
        expect(project.projected_days_remaining).to be_nan
        expect(project).not_to be_on_schedule
      end
    end

    describe 'with a task' do
      let(:project) { build_stubbed(:project, tasks: [task])}
      let(:task) { build_stubbed(:task)}

      it "knows that a project with an incomplete task is not done" do
        expect(project).not_to be_done
      end
      
      it 'marks a project done if its tasks are done' do
        task.mark_completed

        expect(project).to be_done
      end
    end

  end

  describe 'estimates' do
    # Arrange
    let(:project) { build_stubbed(:project,
      tasks: [newly_done, old_done, small_not_done, large_not_done])}

    let(:newly_done) { build_stubbed(:task, :newly_complete) }
    let(:old_done) { build_stubbed(:task, :long_complete, :small) }

    let(:small_not_done) { build_stubbed(:task, :small) }
    let(:large_not_done) { build_stubbed(:task, :large) }


    # Assert
    it 'can calculate total size' do
      done_tasks_sizes = newly_done.size + old_done.size + small_not_done.size + large_not_done.size
      expect(project.size).to eq(done_tasks_sizes)
    end

    it 'can calculate total project size' do
      expect(project).to be_of_size(10)
      expect(project).not_to be_of_size(5)
    end

    it 'can calculate remaining size' do
      not_done_tasks_sizes = small_not_done.size + large_not_done.size
      expect(project.remaining_size).to eq(not_done_tasks_sizes)
    end

    it 'can calculate remaining project size' do
      expect(project).to be_of_size(6).for_incomplete_tasks_only
    end

    it 'knows its velocity' do
      done_tasks_in_3wk_range = newly_done.size
      expect(project.completed_velocity).to eq(done_tasks_in_3wk_range)
    end

    it 'knows its rate' do
      # (newly_done.size * 1.0) / (3weeks in days)
      calc_rate = (3 * 1.0) / (3 * 7)
      expect(project.current_rate).to eq(calc_rate)
    end

    it 'knows its projected days remaining' do
      #     not_done_tasks_sizes / current_rate
      projected_days = (1 + 5) / ((3 * 1.0) / (3 * 7))
      expect(project.projected_days_remaining).to eq(projected_days)
    end

    it 'knows if it is not on schedule' do
      project.due_date = 1.week.from_now

      expect(project).not_to be_on_schedule
    end

    it 'knows if it is on schedule' do
      project.due_date = 6.months.from_now

      expect(project).to be_on_schedule
    end

  end

  describe 'task order' do
    let(:project) { create(:project, name:'Project')}

    it 'makes 1 the order of the first task in an entry project' do
      expect(project.next_task_order).to eq(1)
    end

    it 'gives the order of the next task as one more than the highest' do
      project.tasks.create(project_order:1)
      project.tasks.create(project_order:3)
      project.tasks.create(project_order:2)
      
      expect(project.next_task_order).to eq(4)
    end

  end
end