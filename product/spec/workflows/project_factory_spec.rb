require 'rails_helper'

RSpec.describe ProjectFactory do 
  describe "initialization" do
    it 'creates a project given a name' do
      factory = ProjectFactory.new(name: 'Project Runway')
      factory.build
      expect(factory.project.name).to eq('Project Runway')
    end
  end
  
  describe "task string parsing" do
    it 'handles an empty string' do
      factory = ProjectFactory.new(name: 'Project Runway', task_string: "")
      tasks = factory.convert_string_to_tasks
      expect(tasks).to be_empty 
    end
    
    it 'handles a single string' do
      factory = ProjectFactory.new(
        name: 'Project Runway', 
        task_string: "Start Things")
      tasks = factory.convert_string_to_tasks
      expect(tasks.size).to eq(1)
      expect(tasks.first).to have_attributes(title: 'Start Things', size: 1)
    end

    it 'handles a single string with size' do
      factory = ProjectFactory.new(
        name: 'Project Runway', 
        task_string: 'Start Things:3')
      tasks = factory.convert_string_to_tasks
      expect(tasks.size).to eq(1)
      expect(tasks.first).to have_attributes(title: 'Start Things', size: 3)
    end
    
    it 'handles a single string with size zero' do
      factory = ProjectFactory.new(
        name: 'Project Runway', 
        task_string: 'Start Things:0')
      tasks = factory.convert_string_to_tasks
      expect(tasks.size).to eq(1)
      expect(tasks.first).to have_attributes(title: 'Start Things', size: 1)
    end
    
    it 'handles a single string with malformed size' do
      factory = ProjectFactory.new(
        name: 'Project Runway', 
        task_string: 'Start Things:')
      tasks = factory.convert_string_to_tasks
      expect(tasks.size).to eq(1)
      expect(tasks.first).to have_attributes(title: 'Start Things', size: 1)
    end
    
    it 'handles a single string with negative size' do
      factory = ProjectFactory.new(
        name: 'Project Runway', 
        task_string: 'Start Things:-1')
      tasks = factory.convert_string_to_tasks
      expect(tasks.size).to eq(1)
      expect(tasks.first).to have_attributes(title: 'Start Things', size: 1)
    end
    
    it 'handles multiple tasks' do
      factory = ProjectFactory.new(
        name: 'Project Runway', 
        task_string: 'Start Things:3\nEnd Things:2')
      tasks = factory.convert_string_to_tasks
      expect(tasks.size).to eq(2)
      expect(tasks).to match([
        an_object_having_attributes(title: 'Start Things', size: 3),
        an_object_having_attributes(title: 'End Things', size: 2)
      ])
    end
    
    it 'attaches tasks to the project' do
      factory = ProjectFactory.new(
        name: 'Project Runway', 
        task_string: 'Start Things:3\nEnd Things:2')
      factory.create
      expect(factory.project.tasks.size).to eq(2)
      expect(factory.project).not_to be_a_new_record
    end
  end

  describe "failure cases" do
    it 'fails when trying to save a project with no name' do
      factory = ProjectFactory.new(name: '', task_string: '')
      factory.create
      expect(factory).not_to be_a_success
    end
  end
  
end